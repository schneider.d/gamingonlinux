Options +SymLinksifOwnerMatch

  RewriteEngine On

  RewriteBase /

  ErrorDocument 401 default
  ErrorDocument 403 default
  ErrorDocument 404 /index.php?module=404
  ErrorDocument 500 /down.html
  
  #fonts
  AddType application/font-sfnt            otf ttf
  AddType application/font-woff            woff
  AddType application/font-woff2           woff2
  AddType application/vnd.ms-fontobject    eot

  # BEGIN EXPIRES
<IfModule mod_expires.c>
    ExpiresActive On
    ExpiresByType text/css "access plus 1 year"
    ExpiresByType text/plain "access plus 1 month"
    ExpiresByType image/gif "access plus 1 year"
    ExpiresByType image/png "access plus 1 year"
    ExpiresByType image/webp "access plus 1 year"
    ExpiresByType image/jpeg "access plus 1 year"
    ExpiresByType image/svg+xml "access plus 1 year"
    ExpiresByType image/vnd.microsoft.icon "access plus 1 year"
    ExpiresByType image/x-icon "access plus 1 year"
    ExpiresByType application/x-javascript "access plus 1 year"
    ExpiresByType application/javascript "access plus 1 year"
    ExpiresByType application/x-icon "access plus 1 year"
    ExpiresByType application/font-woff "access plus 1 year" 
    ExpiresByType application/font-woff2 "access plus 1 year"
    ExpiresByType application/font-sfnt "access plus 1 year"
    ExpiresByType application/vnd.ms-fontobject "access plus 1 year"
    ExpiresByType application/rss+xml "access plus 1 hour"
</IfModule>
# END EXPIRES

# Extra Security Headers

<IfModule mod_headers.c>

	Header set X-XSS-Protection "1; mode=block"

	Header always append X-Frame-Options SAMEORIGIN

	Header set X-Content-Type-Options nosniff

	Header set Strict-Transport-Security "max-age=10886400; includeSubDomains; preload"

</IfModule>

  # force https
  # RewriteCond %{HTTPS} off
  # RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]  

  # force www.
  # RewriteCond %{HTTP_HOST} !^www\.
  # RewriteRule ^(.*)$ https://www.%{HTTP_HOST}/$1 [R=301,L]

  # force trailing slash
  RewriteCond %{REQUEST_URI} !^/wiki/
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)([^/])$        /$1$2/ [L,R=301]

  # general page rules to look prettier
  RewriteRule ^image_prox/([%\w\.-_]*)$ /includes/image_proxy.php?url=$1 [N,NC]

  RewriteRule ^/?wiki(/.*)?$ %{DOCUMENT_ROOT}/w/index.php [L]

  RewriteRule ^calendar/$ /index.php?module=calendar [L]

  RewriteRule ^mailinglist/$ /index.php?module=mailing_list [L]

  RewriteRule ^latest-comments/$ /index.php?module=comments_latest [L]

  RewriteRule ^latest-comments/page=([0-9]+)/$ /index.php?module=comments_latest&page=$1 [L,NC,QSA]

  RewriteRule ^irc/$ /index.php?module=irc [L]

  RewriteRule ^register/$ /index.php?module=register [L]

  RewriteRule ^about-us/$ /index.php?module=about_us [L]

  RewriteRule ^private-messages/$ /index.php?module=messages [L]

  RewriteRule ^submit-article/$ /index.php?module=submit_article&view=Submit [L]

  RewriteRule ^submit-article/error\=(.+?)?$ /index.php?module=submit_article&view=Submit&error=$1 [L,NC,QSA]

  RewriteRule ^contact-us/$ /index.php?module=contact [L]

  RewriteRule ^email-us/$ /index.php?module=email_us [L]

  RewriteRule ^support-us/$ /index.php?module=support_us [L]

  RewriteRule ^articles/(.+?)\.([0-9]+)/$ /index.php?module=articles_full&title=$1&aid=$2 [L,NC,QSA]

  RewriteRule ^articles/([0-9]+)/$ /index.php?module=articles_full&aid=$1 [L,NC,QSA]

  RewriteRule ^articles/(.+?)\.([0-9]+)/error=(.+?)/?$ /index.php?module=articles_full&aid=$2&title=$1&error=$3 [L,NC,QSA]

  RewriteRule ^articles/(.+?)\.([0-9]+)/page\=([0-9]+)/?$ /index.php?module=articles_full&aid=$2&title=$1&page=$3 [L,NC,QSA]

  RewriteRule ^articles/(.+?)\.([0-9]+)/comment_id\=([0-9]+)/?$ /index.php?module=articles_full&aid=$2&title=$1&comment_id=$3 [L,NC,QSA]

  RewriteRule ^articles/(.+?)\.([0-9]+)/article_page\=([0-9]+)/?$ /index.php?module=articles_full&aid=$2&title=$1&article_page=$3 [L,NC,QSA]

  RewriteRule ^private-messages/page\=([0-9]+)/$ /index.php?module=messages&page=$1 [L,NC,QSA]

  RewriteRule ^private-messages/compose/(\d+)*/$ /index.php?module=messages&view=compose [L]

  RewriteRule ^private-messages/([0-9]+)/$ /index.php?module=messages&view=message&id=$1 [L,NC,QSA]

  RewriteRule ^private-messages/([0-9]+)/page\=([0-9]+)/$ /index.php?module=messages&view=message&id=$1&page=$2 [L,NC,QSA]

  RewriteRule ^profiles/([0-9]+)/$ /index.php?module=profile&user_id=$1 [L,NC,QSA]

  RewriteRule ^profiles/([0-9]+)/comments/page\=([0-9]+)$ /index.php?module=profile&view=more-comments&user_id=$1&page=$2 [L,NC,QSA]

  RewriteRule ^users/statistics/$ /index.php?module=statistics [L,NC,QSA]

  RewriteRule ^users/statistics/statid=([0-9]+)/$ /index.php?module=statistics&statid=$1&act=pick [L,NC,QSA]

  RewriteRule ^articles/category/(.+?)/page\=([0-9]+)/$ /index.php?module=articles&view=cat&catid=$1&page=$2 [L,NC,QSA,B]

  RewriteRule ^articles/category/(.+?)/$ /index.php?module=articles&view=cat&catid=$1 [L,NC,QSA,B]

  RewriteRule ^itemdb/([0-9]+)/$ /index.php?module=items_database&view=item&id=$1 [L,NC,QSA]

  RewriteRule ^itemdb/developer/([0-9]+)/$ /index.php?module=items_database&view=developer&id=$1 [L,NC,QSA]

  RewriteRule ^itemdb/anticheats/([0-9]+)/$ /index.php?module=items_database&view=anticheats&id=$1 [L,NC,QSA]

  RewriteRule ^steam-tracker/$ /index.php?module=steam_linux_share [L,NC,QSA]

  RewriteRule ^steamplay/$ steamplay.php [L,NC,QSA]

  RewriteRule ^steamplay/reports/([0-9]+)/$ steamplay_reports.php?id=$1 [L,NC,QSA]

  RewriteRule ^steamplay/reports/([0-9]+)/page=([0-9]+)/$ steamplay_reports.php?id=$1&page=$2 [L,NC,QSA]

  RewriteRule ^db/$ itemdb.php [L,NC,QSA]

  RewriteRule ^free-games/$ free_games.php [L]

  RewriteRule ^itemdb/steamid/([0-9]+)/$ itemdb.php?steamid=$1 [L,NC,QSA]

  RewriteRule ^sales/page=([0-9]+)/$ /sales.php?page=$1 [L,NC,QSA]

  RewriteRule ^sales/([0-9]+)/$ /sales.php?sale_id=$1 [L,NC,QSA]

  RewriteRule ^sales/message=(.+?)/?$ /sales.php?message=$1 [L,NC,QSA]

  RewriteRule ^sales/$ sales.php [L]

  Redirect 301 /podcast_rss_ogg.php /podcast_rss.php?format=ogg

  Redirect 301 /podcast_rss_mp3.php /podcast_rss.php?format=mp3
  
  # if requested url does not exist pass it as path info to index.php
  RewriteRule ^$ index.php?/ [QSA,L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule (.*) index.php?/$1 [QSA,L]
