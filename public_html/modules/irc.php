<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'GamingOnLinux IRC Chat', 1);
$templating->set_previous('meta_description', 'GamingOnLinux.com IRC Chat', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'irc/">', 1);

$templating->load('irc');

$templating->block('irc_main');
