<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'Email Us', 1);
$templating->set_previous('meta_description', 'Email form to contact GamingOnLinux.com', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'email-us/">', 1);

$templating->load('email_us');

$templating->block('top');
$templating->set('discord_link', $core->config('discord'));

$submit_article_text = '';
if (isset($_SESSION['user_id']) && $_SESSION['user_id'] > 0)
{
	$submit_article_text = 'If you are submitting a full article please use the <a href="/submit-article/">Submit Article area!</a> ';
}
$templating->set('submit_article_text', $submit_article_text);
$templating->block('main', 'email_us');
$templating->set('name', $name);
$templating->set('email', $email);
$templating->set('message', $message);
$templating->set('url', url);
?>
