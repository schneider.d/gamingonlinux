<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'GamingOnLinux RSS Feeds', 1);
$templating->set_previous('meta_description', 'GamingOnLinux.com RSS Feeds', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'rss/">', 1);

$custom_url = [];
$templating->load('rss');
$templating->block('rss_main');
$options = '';
$res = $dbl->run("SELECT `category_id`, `category_name` FROM `articles_categorys` ORDER BY `category_name` ASC")->fetch_all();
foreach ($res as $get_cats)
{
    if (isset($_GET['tags']) && is_array($_GET['tags']))
    {
        $selected = '';
        if (in_array($get_cats['category_id'], $_GET['tags']))
        {
            $selected = 'selected';
        }
    }
    $options .= '<option value="'.$get_cats['category_id'].'" '.$selected.'>'.$get_cats['category_name'].'</option>';
}
$templating->set('options', $options);

$all_check = '';
$any_check = 'checked';

$templating->set('any_check', $any_check);
$templating->set('all_check', $all_check);

if (isset(($_GET['view'])) && $_GET['view'] == 'generate')
{
    if (isset($_GET['tags']))
	{
		$tags = $_GET['tags'];

		if (!is_array($tags))
		{
            $_SESSION['message'] = 'empty';
            $_SESSION['message_extra'] = 'tags list';
            header("Location: ".url."/rss/");
            die();
		}
		if (!core::is_number($tags))
		{
            $_SESSION['message'] = 'empty';
            $_SESSION['message_extra'] = 'tags list';
            header("Location: ".url."/rss/");
            die();
		}

        foreach($tags as $tag)
        {
            $custom_url[] = 'tags[]='.$tag;
        }

        $show = '';
        if (isset($_GET['type']) && $_GET['type'] == 'all')
        {
            $show = '&all';
        }
        if (isset($_GET['type']) && $_GET['type'] == 'any')
        {
            $show = '&any';
        }

        $templating->block('generated_url');
        $templating->set('website_url', url);
        $templating->set('custom_url', implode('&', $custom_url).$show);

    }
    else
    {
        $_SESSION['message'] = 'empty';
        $_SESSION['message_extra'] = 'tags list';
        header("Location: ".url."/rss/");
        die();
    }
}