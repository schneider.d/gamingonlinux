<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('title', 'Game giveaways', 1);
$templating->set_previous('meta_description', 'GamingOnLinux game giveaways', 1);

$templating->load('giveaways');

if (isset($_GET['id']))
{
	$giveaway_id = (int) $_GET['id'];

	$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'index.php?module=giveaways&amp;id='.$giveaway_id.'/">', 1);
	$text = "[giveaway]".$_GET['id']."[/giveaway]";

	$text = $bbcode->replace_giveaways($text, $_GET['id'], 1);

	$templating->block('top');
	$templating->set('text', $text);
}
else
{
	$core->message("You need to provide a giveaway ID.");
}