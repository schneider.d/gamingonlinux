<?php
if(!defined('golapp')) 
{
	die('Direct access not permitted');
}
$templating->set_previous('meta_description', 'The hottest articles on GamingOnLinux over 30 days', 1);
$templating->set_previous('title', 'Hottest GamingOnLinux Articles', 1);
$templating->set_previous('canonical_link', '<link rel="canonical" href="'.$core->config('website_url') . 'hot-articles/">', 1);

$templating->load('hot_articles');
$templating->block('top', 'hot_articles');

/*
// top articles this month but not from the most recent 2 days to prevent showing what they've just seen on the home page
*/
$blocked_tags  = str_repeat('?,', count($user->blocked_tags) - 1) . '?';
$top_article_query = "SELECT a.`article_id`, a.`title`, a.`slug`, a.`date`, a.`tagline_image`, a.`gallery_tagline`, a.comment_count, a.author_id, a.guest_username, t.`filename` as gallery_tagline_filename, u.`username`, u.`profile_address` FROM `articles` a LEFT JOIN `articles_tagline_gallery` t ON t.`id` = a.`gallery_tagline` LEFT JOIN `users` u ON a.author_id = u.user_id WHERE a.`date` > UNIX_TIMESTAMP(NOW() - INTERVAL 1 MONTH) AND a.`views` > ? ORDER BY a.`views` DESC LIMIT 20";

$fetch_top = $dbl->run($top_article_query, array($core->config('hot-article-viewcount')))->fetch_all();

$article_id_array = array();

foreach ($fetch_top as $article)
{
	$article_id_array[] = $article['article_id'];
}

$article_id_sql = implode(', ', $article_id_array);

$get_categories = $article_class->find_article_tags(array('article_ids' => $article_id_sql, 'limit' => 5));

$article_class->display_article_list($fetch_top, $get_categories);
