<?php
error_reporting(E_ALL);

if (!class_exists('Redis'))
{
    echo 'Redis not online for PHP!';
}

$redis = new Redis();
try {
    $redis->connect('127.0.0.1', 6379, 0.01);
}  catch (Exception $e) {
    $msg = strtolower($e->getMessage());
    $this->assertEquals('connection timed out', $msg);
}

echo $redis->ping('Online');
?>
