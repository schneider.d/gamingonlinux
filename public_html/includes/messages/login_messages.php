<?php
// livestreams submission page
return [ 
	"password_match" => [
		"text" => "It looks like the password you entered didn't match what we have!",
		"error" => 1
	],
	"bad_username" => [
		"text" => "We couldn't find that username!",
		"error" => 1
	],
	"no_password" => [
		"text" => "You need to actually enter a password dummy!",
		"error" => 1
	],
	"completed_reset" => [
		"text" => "If a user exists with that email, they will get further instructions on how to reset in their inbox."
	],
	"reset_locked" => [
		"text" => "Due to repeated login reset attempts, this has been locked. Please try again in a few days.",
		"error" => 1
	],
	"limits_hit" => [
		"text" => "Checker limit reached, please try again later.",
		"error" => 1
	],
	"captcha_nope" =>
	[
		"text" => "You did not correctly fill out the captcha.",
		"error" => 1
	]
];
