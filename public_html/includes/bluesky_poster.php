<?php
if(!defined('golapp'))
{
	die('Direct access not permitted');
}

$config['bluesky-username']="gamingonlinux.com";

$text=BLUESKY_POST_TITLE;

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://bsky.social/xrpc/com.atproto.server.createSession',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "identifier":"'.$config['bluesky-username'].'",
    "password":"'.BLUESKY_API_PASSWORD.'"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);
curl_close($curl);
$session=json_decode($response,TRUE);

// That's got the auth bearer, and other bits of session data
// So now we need to post the message

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://bsky.social/xrpc/com.atproto.repo.createRecord',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "repo":"'.$session['did'].'",
    "collection":"app.bsky.feed.post",
    "record":{
        "$type":"app.bsky.feed.post",
        "createdAt":"'.date("c").'",
        "text":"'.$text.'",
		"embed":{
			"$type": "app.bsky.embed.external",
			"external":{
				"uri": "'.BLUESKY_POST_LINK.'",
				"title": "'.BLUESKY_POST_TITLE.'",
				"description": "'.BLUESKY_POST_DESCRIPTION .'"
			}
		}
    }
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'Authorization: Bearer '.$session['accessJwt']
  ),
));

$response = curl_exec($curl);

curl_close($curl);
?>
