User-agent: *
Disallow: /submit-article/
Disallow: /usercp.php
Disallow: /admin.php

Sitemap: https://www.gamingonlinux.com/sitemap.xml
