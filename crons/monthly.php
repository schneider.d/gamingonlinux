<?php

define("APP_ROOT", dirname( dirname(__FILE__) ) . '/public_html');

require APP_ROOT . "/includes/bootstrap.php";

$prev_month = date('n', strtotime('-1 months'));
$year_selector = date('Y');
if ($prev_month == 12)
{
	$time = strtotime("-1 year", time());
	$year_selector = date("Y", $time);
}
$first_minute = mktime(0, 0, 0, $prev_month, 1, $year_selector);
$last_minute = mktime(23, 59, 59, $prev_month, date("t"), $year_selector);

$counter = $dbl->run("SELECT count(DISTINCT a.article_id) as `counter` FROM `articles` a LEFT JOIN `article_category_reference` c ON a.article_id = c.article_id WHERE a.`date` >= $first_minute AND a.`date` <= $last_minute AND c.category_id NOT IN (63) AND a.active = 1")->fetchOne();

$prevdate = date('F Y', strtotime('-1 months'));

$title = "What was hot on GamingOnLinux through $prevdate, {$counter} in total";

$tagline = "Here is a look back some of the most popular articles on GamingOnLinux for $prevdate, an easy way to for you to keep up to date on what has happened in the past month for Linux gaming, open source and other general Linux news that we cover!";

$text = "<p>Here is a look back some of the most popular articles on GamingOnLinux for $prevdate, an easy way to for you to keep up to date on what has happened in the past month for Linux gaming, open source and other general Linux news that we cover! If you wish to keep track of these overview posts you can with our <a href=\"http://www.gamingonlinux.com/article_rss.php?section=overviews\">Overview RSS</a>. Giving you a place to have some catch-up and a place for chat in the comments.</p>

<p><strong>Here's what was popular with our readers:</strong></p>";

// sub query = grab the highest ones, then the outer query sorts them in ascending order, so we get the highest viewed articles, and then sorted from lowest to highest
$get_articles = $dbl->run("SELECT a.`article_id`,a.`title`,a.`tagline`,a.`views`,a.`date`,a.`slug` FROM articles a LEFT JOIN `article_category_reference` c ON a.`article_id` = c.`article_id` WHERE a.`date` >= $first_minute AND a.`date` <= $last_minute AND c.`category_id` NOT IN (63, 92) AND a.`active` = 1 group by `a`.`article_id` ORDER BY a.`views` DESC LIMIT 30")->fetch_all();

$text .= '<ul>';

foreach ($get_articles as $articles)
{
	$article_link = $article_class->article_link($articles);
	$text .= '<li><a href="'.$article_link.'">' . $articles['title'] . '</a>';
	$text .= '<ul><li>' . $articles['tagline'] . '</li></ul></li>';
}

$text .= '</ul>';

$text .= "<p>We published a total of <strong>{$counter} articles last month</strong>! You can see who <a href=\"/index.php?module=website_stats\">contributed articles on this page.</a></p>
<p>Thanks again to all of our supporters, who enable us to do this entirely free of adverts and full time. See how you can help <a href=\"/support-us/\">support us on this dedicated page</a>.</p>";

$text .= "<p>What was your favourite Linux and Gaming news through $prevdate?</p>";

echo 'Title: ' . $title . PHP_EOL;

echo $text;

$slug = core::nice_title($title);

//$dbl->run("INSERT INTO `articles` SET `author_id` = 1844, `date` = ?, `title` = ?, `slug` = ?, `tagline` = ?, `text` = ?, `show_in_menu` = 0, `tagline_image` = 'monthlyoverview.png', `active` = 0, `admin_review` = 1", array(core::$date, $title, $slug, $tagline, $text));

$article_id = $dbl->new_id();

//$dbl->run("INSERT INTO `article_category_reference` SET `article_id` = ?, `category_id` = 63", array($article_id));

// update admin notifications
//$dbl->run("INSERT INTO `admin_notifications` SET `user_id` = 1844, `completed` = 0, `type` = ?, `created_date` = ?, `data` = ?", array('article_admin_queue', core::$date, $article_id));

?>