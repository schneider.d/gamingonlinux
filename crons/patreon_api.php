<?php
define("APP_ROOT", dirname( dirname(__FILE__) ) . '/public_html');

require APP_ROOT . "/includes/bootstrap.php";
require_once(APP_ROOT . '/includes/patreon/API.php');
require_once(APP_ROOT . '/includes/patreon/OAuth.php');
 
use Patreon\API;
use Patreon\OAuth;

$client_id = $core->config('patreon_client_id');
$client_secret = $core->config('patreon_client_secret');
$creator_access_key = $core->config('patreon_creator_access_token');
$refresh_token = $core->config('patreon_creator_refresh_token');

// Test if access token is right, as Patreon like to expire the access token every month! Yes it's terrible.
// Also seems it gives a new Refresh token too, so we also need to store the new one there with the new access token because *patreon reasoning*
$api_client = new Patreon\API( $creator_access_key );

// Get your campaign data
$campaign_response = $api_client->fetch_campaigns();

if (!is_array($campaign_response))
{
	$campaign_response = json_decode($campaign_response, true);
}

// this means Patreon expired the creator access token, thanks patreon!
if (isset($campaign_response['errors'])) 
{
	$oauth_client = new Patreon\OAuth($client_id, $client_secret);
	$tokens = $oauth_client->refresh_token($refresh_token, null);
	$core->set_config($tokens['access_token'], 'patreon_creator_access_token');
	$core->set_config($tokens['refresh_token'], 'patreon_creator_refresh_token');
	// note the date it expires
	$date = new DateTime("now");
	$expires = $date->modify("+".$tokens['expires_in']." seconds");
	$core->set_config($expires->format('Y-m-d H:i:s'), 'patreon_creator_access_expiry');
}

// if it's close to expiring get a new one
$expiry = $core->config('patreon_creator_access_expiry');
$seconds_to_expire = strtotime($expiry) - time();

if ($seconds_to_expire < 3*86400)
{
	$oauth_client = new Patreon\OAuth($client_id, $client_secret);
	$tokens = $oauth_client->refresh_token($refresh_token, null);
	$core->set_config($tokens['access_token'], 'patreon_creator_access_token');
	$core->set_config($tokens['refresh_token'], 'patreon_creator_refresh_token');
	// note the date it expires
	$date = new DateTime("now");
	$expires = $date->modify("+".$tokens['expires_in']." seconds");
	$core->set_config($expires->format('Y-m-d H:i:s'), 'patreon_creator_access_expiry');	
}